package;

import haxe.ui.HaxeUIApp;

class Main {
	public static function main() {
		Translations.load();
		var app = new HaxeUIApp();
		app.ready(function() {
			app.addComponent(new MainView());

			app.title = "Install " + Sys.args().join(" ");

			app.start();
		});
	}
}
