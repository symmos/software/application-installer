package;

import haxe.ui.containers.VBox;
import haxe.ui.events.MouseEvent;

@:build(haxe.ui.ComponentBuilder.build("assets/main-view.xml"))
class MainView extends VBox {
	public function new() {
		super();

		cancel.text = Translations.get("cancel");
		install.text = Translations.get("install");

		cancel.onClick = _ -> Sys.exit(0);
		install.onClick = _ -> {
			Sys.command("xfce4-terminal -x sudo pacman -U " + Sys.args().join("\\ "));
			Sys.exit(0);
		}
	}
}
